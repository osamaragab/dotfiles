# dotfiles

> config files and scripts.

## Setup

> NOTE: The scripts may override your files; backup your files and read the
code before running it.

Clone the repo and run the `setup` script:
```sh
git clone https://gitlab.com/osamaragab/dotfiles.git
cd dotfiles
./setup
```

Use the `sysinit` script instead to install the packages and setup the dotfiles
for a fresh [Void Linux](https://voidlinux.org/) install:
```sh
./sysinit
```
